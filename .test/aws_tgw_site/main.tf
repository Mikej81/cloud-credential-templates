# terraform init
terraform {
  backend "s3" {
    region = "us-east-1"
    encrypt = true
  }
  required_providers {
    volterra = {
      source = "volterraedge/volterra"
    }
  }
}

# variables
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}

variable "aws_tgw_spoke_vpc_cidr" {
  default = {
    0 = "10.10.0.0/16"
    1 = "10.20.0.0/16"
  }
}

variable "password" {}
variable "policy_name" {}
variable "name" {}
variable "ssh_key" {
  default = ""
}

variable "api_p12_file" {}
variable "api_url" {}

# providers
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

provider "volterra" {
  api_p12_file = var.api_p12_file
  url          = var.api_url
}

# local variables
locals {
  aws_access_key = aws_cloudformation_stack.stack.outputs.AccessKey
  aws_secret_key = aws_cloudformation_stack.stack.outputs.SecretKey
}

# resources
resource "aws_cloudformation_stack" "stack" {
  name          = format("%s-stack", var.name)
  template_body = file("${path.module}/../../aws/aws-tgw-site-service-account.yaml")
  parameters    = {
    "Password"   = var.password
    "PolicyName" = var.policy_name
  }
  capabilities  = ["CAPABILITY_NAMED_IAM"]

  lifecycle {
    ignore_changes = [ parameters ]
  }
}

# Create spoke VPCs to be attached to the AWS TGW site
resource "aws_vpc" "aws_tgw_spoke_vpc" {
  count            = 2
  cidr_block       = lookup(var.aws_tgw_spoke_vpc_cidr, count.index)
  instance_tenancy = "default"

  tags = {
    Name  = format("%s-%s", var.name, count.index)
  }
}

# Create VPC subnets in the spoke VPCs
resource "aws_subnet" "aws_tgw_spoke_vpc_subnet" {
  count         = 2
  vpc_id        = aws_vpc.aws_tgw_spoke_vpc[count.index].id
  cidr_block    = cidrsubnet(lookup(var.aws_tgw_spoke_vpc_cidr, count.index), 8, 0)

  tags = {
    Name  = format("%s-subnet-%s", var.name, count.index)
  }
}

resource "volterra_cloud_credentials" "aws_creds" {
  name          = format("%s-creds", var.name)
  namespace     = "system"
  aws_secret_key {
    access_key = local.aws_access_key
    secret_key {
      clear_secret_info {
        url = "string:///${base64encode(local.aws_secret_key)}"
      }
    }
  }
}

resource "volterra_aws_tgw_site" "site" {
  depends_on    = [aws_vpc.aws_tgw_spoke_vpc, aws_subnet.aws_tgw_spoke_vpc_subnet]
  name          = var.name
  namespace     = "system"
  description   = "f5xc aws tgw site for cloud-credentials"

  aws_parameters {
    aws_certified_hw  = "aws-byol-multi-nic-voltmesh"
    aws_region        = var.aws_region
    ssh_key           = var.ssh_key
    disk_size         = "40"
    no_worker_nodes   = true
    instance_type     = "t3.xlarge"

    new_vpc {
      name_tag      = format("%s-vpc", var.name)
      primary_ipv4  = "192.168.0.0/22"
    }

    new_tgw {
      system_generated = true
    }

    az_nodes {
      aws_az_name             = format("%sa", var.aws_region)
      disk_size               = "80"
      reserved_inside_subnet  = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.0.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.0.128/25"
        }
      }
    }
    az_nodes {
      aws_az_name             = format("%sb", var.aws_region)
      disk_size               = "80"
      reserved_inside_subnet  = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.1.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.1.128/25"
        }
      }
    }
    az_nodes {
      aws_az_name             = format("%sc", var.aws_region)
      disk_size               = "80"
      reserved_inside_subnet  = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.2.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.2.128/25"
        }
      }
    }

    aws_cred {
      name      = volterra_cloud_credentials.aws_creds.name
      namespace = "system"
    }
  }

  vpc_attachments {
    dynamic "vpc_list" {
      for_each  = toset(aws_vpc.aws_tgw_spoke_vpc)
      content {
        vpc_id = vpc_list.value.id
      }
    }
  }

  vn_config {
    no_inside_static_routes     = true
    no_outside_static_routes    = true
    no_global_network           = true
    no_dc_cluster_group         = true
    sm_connection_public_ip     = true
  }

  tgw_security {
    no_forward_proxy                    = true
    east_west_service_policy_allow_all  = true
    no_network_policy                   = true
  }

  direct_connect_enabled {
    standard_vifs = true
    auto_asn      = true
  }

  offline_survivability_mode {
    no_offline_survivability_mode = true
  }

  performance_enhancement_mode {
    perf_mode_l7_enhanced = true
  }

  default_blocked_services  = true
  logs_streaming_disabled   = true

  lifecycle {
    ignore_changes = [ labels,description ]
  }
}

resource "volterra_tf_params_action" "apply_site" {
  depends_on      = [
    volterra_aws_tgw_site.site,
    volterra_cloud_credentials.aws_creds,
    aws_subnet.aws_tgw_spoke_vpc_subnet,
    aws_vpc.aws_tgw_spoke_vpc,
    aws_cloudformation_stack.stack
  ]
  site_name       = var.name
  site_kind       = "aws_tgw_site"
  action          = "apply"
  wait_for_action = true
}

# outputs
output "volterra_tf_params_action" {
  value = volterra_tf_params_action.apply_site
}