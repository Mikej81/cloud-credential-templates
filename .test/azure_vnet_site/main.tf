# terraform init
terraform {
  backend "s3" {
    region = "us-east-1"
    encrypt = true
  }
  required_providers {
    volterra = {
      source = "volterraedge/volterra"
    }
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.62.1"
    }
    azuread = {
      source = "hashicorp/azuread"
      version = "2.39.0"
    }
  }
}

# variables

## azure access
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "ssh_key" {
  default = ""
}

## voltconsole access
variable "api_p12_file" {}
variable "api_url" {}

## volterra config
variable "name" {}
variable "azure_region" {}

# providers

## azure resource manager
provider "azurerm" {
  features {}
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
}

# provider "azurerm" {
#   features {}
#   subscription_id = var.subscription_id
#   client_id       = local.client_id
#   client_secret   = local.client_secret
#   tenant_id       = var.tenant_id
#   alias           = "custom"
# }

## azure active directory
provider "azuread" {
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
}

## volterra
provider "volterra" {
  api_p12_file = var.api_p12_file
  url          = var.api_url
}

# local variables
locals {
  azure_role_actions = jsondecode(file("${path.module}/../../azure/f5xc-azure-custom-role.json"))["Actions"]
  client_id          = azuread_application.app.application_id
  tenant_id          = var.tenant_id
  client_secret      = azuread_application_password.app_pass.value
}

# data sources
data "azurerm_subscription" "ves_plm" {}

# resources
## create role
resource "azurerm_role_definition" "role" {
  name        = var.name
  scope       = data.azurerm_subscription.ves_plm.id
  description = "F5XC Custom Role to create Azure VNET site"

  permissions {
    actions     = local.azure_role_actions
    not_actions = []
  }

  assignable_scopes = [
    data.azurerm_subscription.ves_plm.id
  ]
}

## create an application
resource "azuread_application" "app" {
  depends_on                 = [azurerm_role_definition.role]
  display_name               = var.name
}

## create a service principal
resource "azuread_service_principal" "app" {
  depends_on     = [azuread_application.app]
  application_id = azuread_application.app.application_id
}

## assign role to the service principal
resource "azurerm_role_assignment" "app_role" {
  depends_on          = [azurerm_role_definition.role, azuread_service_principal.app]
  scope               = data.azurerm_subscription.ves_plm.id
  role_definition_id  = azurerm_role_definition.role.role_definition_resource_id
  principal_id        = azuread_service_principal.app.id
}

## generate passoword for the application
resource "azuread_application_password" "app_pass" {
  depends_on            = [azuread_application.app, azurerm_role_assignment.app_role]
  application_object_id = azuread_application.app.id
  end_date_relative     = "240h"
}

## create volterra azure credentials
resource "volterra_cloud_credentials" "az_creds" {
  depends_on = [azuread_application_password.app_pass]
  name       = format("%s-creds", var.name)
  namespace  = "system"
  azure_client_secret {
    client_id       = local.client_id
    subscription_id = var.subscription_id
    tenant_id       = local.tenant_id
    client_secret {
      clear_secret_info {
        url = "string:///${base64encode(local.client_secret)}"
      }
    }
  }
}

# # accept azure marketplace agreement
# resource "azurerm_marketplace_agreement" "f5xc_azure_ce" {
#   provider  = azurerm.custom
#   publisher = "volterraedgeservices"
#   offer     = "entcloud_voltmesh_voltstack_node"
#   plan      = "freeplan_entcloud_voltmesh_voltstack_node_multinic"
# }

# resource "null_resource" "accept_agreement" {
#   depends_on = [
#     azuread_service_principal.app,
#     azuread_application_password.app_pass
#   ]
#   provisioner "local-exec" {
#     command = <<-EOD
#       # login to azure using new credentials
#       az login --service-principal -u "${local.client_id}" -p="${local.client_secret}" --tenant "${local.tenant_id}"
#       # accept marketplace agreement
#       az vm image accept-terms --offer "entcloud_voltmesh_voltstack_node" --plan "freeplan_entcloud_voltmesh_voltstack_node_multinic" --publisher "volterraedgeservices"
#     EOD
#   }
# }

## create volterra azure vnet site
resource "volterra_azure_vnet_site" "site" {
  name            = var.name
  namespace       = "system"
  description     = "f5xc azure vnet site for cloud-credentials"
  azure_region    = var.azure_region
  resource_group  = var.name

  azure_cred {
    name      = volterra_cloud_credentials.az_creds.name
    namespace = "system"
  }

  vnet {
    new_vnet {
      name         = var.name
      primary_ipv4 = "192.168.0.0/22"
    }
  }
  disk_size    = 40
  machine_type = "Standard_D3_v2"

  ingress_egress_gw {
    azure_certified_hw = "azure-byol-multi-nic-voltmesh"
    az_nodes {
      azure_az  = 1
      disk_size = 40
      inside_subnet {
        subnet_param {
          ipv4 = "192.168.0.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.0.128/25"
        }
      }
    }
    az_nodes {
      azure_az  = 2
      disk_size = 40
      inside_subnet {
        subnet_param {
          ipv4 = "192.168.1.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.1.128/25"
        }
      }
    }
    az_nodes {
      azure_az  = 3
      disk_size = 40
      inside_subnet {
        subnet_param {
          ipv4 = "192.168.2.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.2.128/25"
        }
      }
    }

    performance_enhancement_mode {
      perf_mode_l7_enhanced = true
    }

    no_network_policy        = true
    no_forward_proxy         = true
    no_inside_static_routes  = true
    no_outside_static_routes = true
    no_global_network        = true
    no_dc_cluster_group      = true
    not_hub                  = true
  }
  ssh_key                 = var.ssh_key
  no_worker_nodes         = true
  logs_streaming_disabled = true

  lifecycle {
    ignore_changes = [labels, description]
  }
}

resource "volterra_tf_params_action" "apply_site" {
  depends_on      = [
    volterra_azure_vnet_site.site,
    volterra_cloud_credentials.az_creds,
    azurerm_role_definition.role,
    azuread_application.app,
    azuread_service_principal.app,
    azurerm_role_assignment.app_role,
    azuread_application_password.app_pass
  ]
  site_name       = var.name
  site_kind       = "azure_vnet_site"
  action          = "apply"
  wait_for_action = true
}

# outputs
output "volterra_tf_params_action" {
  value = volterra_tf_params_action.apply_site
}
